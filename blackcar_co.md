# Flatcar Container Linux Blackcar configuration
[Running on QEMU](https://kinvolk.io/docs/flatcar-container-linux/latest/installing/vms/qemu/)
## Generating a password hash
Example from Flatcar Container Linux of creating a proper password hash using multiple proven tools: 
```bash
# On Debian/Ubuntu (via the package "whois")
mkpasswd --method=SHA-512 --rounds=4096

# OpenSSL (note: this will only make md5crypt.  While better than plantext it should not be considered fully secure)
openssl passwd -1

# Python
python -c "import crypt,random,string; print(crypt.crypt(input('clear-text password: '), '\$6\$' + ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(16)])))"

# Perl (change password and salt values)
perl -e 'print crypt("password","\$6\$SALT\$") . "\n"'
```
## YAML Ignition file
Use your ssh key from .shh/id_rsa.pub on host computer. The keys placed in the Ignition file will be appended to .ssh/authorized_keys in flatcar.
* [Example Ignition Configs](https://github.com/kinvolk/ignition/blob/master/doc/examples.md)
* [Setting up Vim for YAML editing by Arthur Koziel](https://www.arthurkoziel.com/setting-up-vim-for-yaml/index.html)
```yaml
passwd:
  users:
    - name: blackknight
      ssh_authorized_keys:
ssh-rsa AAAAXXXXXXXX....
      home_dir: /home/blackknight
      groups:
        - wheel
        - plugdev
      shell: /bin/bash

```
## Container Linux Config Transpiler
Clone and compile ct
```bash
git clone --branch v0.8.0 https://github.com/flatcar-linux/container-linux-config-transpiler \
cd container-linux-config-transpiler \
make
```
Add to .bashrc
```bash
sudo mv -r container-linux-transpiler /opt/
PATH=$PATH:/opt/container-linux-config-transpiler/bin export PATH
```
Open new terminal and verify ct is recognized and functions using new YAML file.
```bash
ct --pretty --in-file alpha.yaml > alpha.ign
```

## Running Flatcar Ignition file and killing them dead
Run Flatcar using Ignition file.
```bash
./flatcar_production_qemu.sh -i alpha.ign -- -nographic
```
ssh into qemu running Flatcar -l is login name a -p is port on ssh-client.
```bash
ssh -l core -p 2222 localhost
```
Kill qemu machine running Flatcar manually.
```bash
ps -e | grep qemu
sudo kill -9 "Process Id found above"
```
